import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]);
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

document.querySelectorAll('header nav ul a')[0].setAttribute('class', 'active'); //Rend cliquable le premier lien du Menu
//document
//	.querySelector('.logo')
//	.insertAdjacentHTML('beforeend', "<small>les pizzas c'est la vie</small>"); //Affiche Le sous titre "Les pizzas c'est la vie"
document.querySelector('.logo').innerHTML +=
	"<small>les pizzas c'est la vie</small>"; //Affiche Le sous titre "Les pizzas c'est la vie"
document.querySelectorAll('body .newsContainer')[0].setAttribute('style', ''); //Affiche le newsContainer "bienvenue sur PizzaLand"

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas
